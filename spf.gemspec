# frozen_string_literal: true

require_relative 'lib/spf/version'

Gem::Specification.new do |gem|
  gem.name          = 'spf'
  gem.summary       = 'ruby implementation of Sender Policy Framework (SPF) RFC7208'
  gem.authors       = 'venkatagiri'
  gem.homepage      = 'https://gitlab.com/venkatagiri/spf'
  gem.licenses      = ['MIT']
  gem.files         = Dir['lib/**/*']
  gem.version       = SPF::VERSION

  gem.required_ruby_version = '~> 2.5.1'
  gem.add_development_dependency 'bundler', '~> 1.16.6'
  gem.add_development_dependency 'pry', '~> 0.13.1'
  gem.add_development_dependency 'rake', '~> 13.0.6'
  gem.add_development_dependency 'rspec', '~> 3.10.0'
  gem.add_development_dependency 'rubocop', '~> 1.23.0'

  gem.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
