# frozen_string_literal: true

require 'resolv'

module SPF
  # DNS resolver
  class DNS
    def initialize
      @lookups = 0
      @voids = 0
    end

    def getresources(domain, type, ignore_void: false)
      resources = Resolv::DNS.open do |dns|
        dns.getresources(domain, type)
      end

      # p "domain:#{domain}, type:#{type}, resources:#{resources}"
      if !ignore_void && resources.empty?
        @voids += 1
        raise PermError, 'dns voids limit' if @voids > 2
      end

      resources
    end

    def check_limits!
      @lookups += 1
      raise PermError, 'dns lookup limit' if @lookups > 10
    end

    def a(domain)
      getresources(domain, Resolv::DNS::Resource::IN::A).map(&:address).map(&:to_s)
    end

    def aaaa(domain)
      getresources(domain, Resolv::DNS::Resource::IN::AAAA).map(&:address).map(&:to_s)
    end

    def mx(domain)
      getresources(domain, Resolv::DNS::Resource::IN::MX).map(&:exchange).map(&:to_s)
    end

    def ptr(domain)
      getresources(domain, Resolv::DNS::Resource::PTR).map(&:name).map(&:to_s)
    end

    def txt(domain, ignore_void: false)
      getresources(domain, Resolv::DNS::Resource::IN::TXT, ignore_void: ignore_void).map(&:data)
    end

    def spf(domain)
      getresources(domain, 'SPF').map(&:data)
    end
  end
end
