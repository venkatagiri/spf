# frozen_string_literal: true

require 'ipaddr'
require 'pry'

module SPF
  # Errors thrown while validating SPF Policy
  class Error < StandardError; end
  class TempError < Error; end
  class PermError < Error; end

  # Evaluate the SPF policy for the IP and MAILFROM
  # to check if the client is authorized
  class Evaluator
    def check_host(ip, helo, sender, dns = DNS.new)
      @dns = dns
      client_addr = IPAddr.new ip
      @c = ip.downcase
      @i = client_addr.reverse.gsub('.ip6.arpa', '').gsub('.in-addr.arpa', '').split('.').reverse.join('.')
      @s = sender
      @o = sender&.split('@')&.last
      @v = client_addr.ipv4? ? 'in-addr' : 'ip6'
      @h = helo
      @r = Socket.gethostname
      @t = Time.now
      @l, @d = split_email sender, helo

      spf_record = dns_spf @d
      return ['none', ''] if spf_record.nil?

      mechanisms, modifiers = parse_terms spf_record
      result = 'neutral'
      explanation = 'DEFAULT'

      mechanisms.each do |term|
        break if result != 'neutral'

        case term.type
        when :all
          result = term.result
        when :a, :mx, :ptr
          @dns.check_limits!
          result = term.result if term.match?(@d, ip)
        when :include
          @dns.check_limits!
          res, _exp = self.class.new.check_host(ip, term.domain, nil, @dns)
          case res
          when 'pass'
            result = term.result
          when 'fail', 'softfail', 'neutral'
            next
          when /^permerror/, 'none'
            raise PermError, "include permerror: #{res}"
          when /^temperror/
            raise TempError, "include temperror: #{res}"
          end
        when :exists
          @dns.check_limits!
          result = term.result unless @dns.a(term.domain).empty?
        when :ip4, :ip6
          result = term.result if term.ip.include? ip
        end
      end

      modifiers.each do |term|
        case term.type
        when :redirect
          next if result != 'neutral'

          @dns.check_limits!
          res, exp = self.class.new.check_host(ip, term.domain, nil, @dns)
          case res
          when 'pass', 'fail', 'softfail', 'neutral'
            return [res, exp]
          when /^permerror/, 'none'
            raise PermError, "include permerror: #{res}"
          when /^temperror/
            raise TempError, "include temperror: #{res}"
          end
        when :exp
          explanation = term.explanation
        end
      end

      # default to neutral when no matches or fails
      [result, explanation]
    rescue PermError
      ['permerror', explanation]
    rescue TempError
      ['temperror', explanation]
    end

    private

    def dns_spf(domain)
      txt_records = @dns.txt(domain, ignore_void: true)
      spf_records = txt_records.compact.select { |r| spf? r }
      raise PermError, '> 1 spf record' if spf_records.length > 1
      return spf_records.first if spf_records.length == 1
      return nil unless txt_records.empty?

      spf_records = @dns.spf(domain).compact.select { |r| spf? r }
      raise PermError, '> 1 spf record' if spf_records.length > 1
      return spf_records.first if spf_records.length == 1
    end

    def split_email(sender, helo)
      return ['postmaster', helo] unless sender && !sender.empty?

      parts = sender.split '@'
      parts[0] = 'postmaster' if parts[0] && parts[0].empty?

      if parts.length == 2
        parts
      else
        ['postmaster', sender]
      end
    end

    def parse_terms(record)
      raise PermError, 'invalid ascii' unless record.ascii_only?

      parts = record.downcase.split.compact.slice(1..-1) # skip the spf version
      terms = parts.map { |term| Term.new(term, @dns, self) }

      mechanisms, modifiers = terms.partition { |t| MECHANISMS.include?(t.type) }

      raise PermError, 'invalid modifier' if modifiers.length > 2
      raise PermError, 'invalid modifier' if modifiers.select { |m| m.type == :exp }.length > 1
      raise PermError, 'invalid modifier' if modifiers.select { |m| m.type == :redirect }.length > 1

      [mechanisms, modifiers]
    end

    def spf?(record)
      record.downcase == spf_version || record.downcase.start_with?("#{spf_version} ")
    end

    def spf_version
      'v=spf1'
    end
  end
end
