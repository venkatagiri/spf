# frozen_string_literal: true

require 'ipaddr'

module SPF
  # Consants
  QUALIFIERS  = %i[+ ~ ? -].freeze
  MECHANISMS  = %i[all a include mx ptr ip4 ip6 exists].freeze
  MODIFIERS   = %i[exp redirect].freeze
  Q2RES = {
    '+': 'pass',
    '-': 'fail',
    '~': 'softfail',
    '?': 'neutral'
  }.freeze

  # Regexes
  QUALIFIER   = "[#{QUALIFIERS.join}]"
  NAME        = '[[:alpha:]][[[:alnum:]]._-]*'
  DOMAINSPEC  = "[^\/]+"
  CIDR        = "\/[[:digit:]]+"
  MECHANISM   = %r{^(#{QUALIFIER})?(#{NAME})(:)?(#{DOMAINSPEC})?(#{CIDR})?(/#{CIDR})?$}.freeze
  MODIFIER    = /^(#{QUALIFIER})?(#{NAME})(=)(#{DOMAINSPEC})$/.freeze

  # Mechanisms and Modifiers in a SPF record
  class Term
    attr :domain, :type, :qualifier, :ip

    def initialize(term, dns, env)
      if term.match?(MODIFIER)
        _term, @qualifier, @type, @separator, @domainspec, @cidr4, @cidr6 = * MODIFIER.match(term)
      elsif term.match? MECHANISM
        _term, @qualifier, @type, @separator, @domainspec, @cidr4, @cidr6 = * MECHANISM.match(term)
      else
        raise PermError, "invalid term: #{term}"
      end

      @type = @type.to_sym
      @qualifier = (@qualifier || '+').to_sym
      @domain = nil
      @env = env
      @dns = dns

      validate!
    end

    def result
      Q2RES[@qualifier]
    end

    def validate!
      validate_type!
      validate_cidrs!

      case @type
      when :include, :exists, :redirect, :exp, :ptr
        raise PermError, "invalid term: #{self}" if !@cidr4.nil? || !@cidr6.nil?

        validate_domainspec!
      when :ip4
        validate_ip4!
      when :ip6
        validate_ip6!
      when :a, :mx
        validate_domainspec! unless @domainspec.nil?
      when :all
        raise PermError, 'all should not have value' if @separator || @domainspec || @cidr4 || @cidr6
      else
        validate_domainspec!
      end
    end

    def match?(cdomain, cip)
      lookup_domain = domain || cdomain
      client_addr = IPAddr.new cip
      cidr = client_addr.ipv4? ? @cidr4 : @cidr6&.slice(1..-1)
      ips = []

      case @type
      when :a
        ips = client_addr.ipv4? ? @dns.a(lookup_domain) : @dns.aaaa(lookup_domain)
      when :mx
        ips = @dns.mx(lookup_domain).map do |mx|
          client_addr.ipv4? ? @dns.a(mx) : @dns.aaaa(mx)
        end.flatten
        raise PermError, 'mx dns limit' if ips.length > 10
      when :ptr
        rev_addr = client_addr.reverse
        ptrs = @dns.ptr(rev_addr).select { |val| val.end_with? lookup_domain }
        ips = ptrs.map { |ptr| client_addr.ipv4? ? @dns.a(ptr) : @dns.aaaa(ptr) }.flatten
      end

      addrs = ips.map { |ip| IPAddr.new "#{ip}#{cidr}" }
      addrs.any? { |a| a.include? client_addr }
    end

    def explanation
      exps = @dns.txt(domain, ignore_void: true)
      raise if exps.length != 1

      expand exps.first
    rescue StandardError
      'DEFAULT'
    end

    def to_s
      [@qualifier.to_s, @type.to_s, @separator, @domainspec, @cidr4, @cidr6].join
    end

    private

    def validate_type!
      raise PermError, "invalid mechanism: #{@type}" if @separator != '=' && !MECHANISMS.include?(@type)

      raise PermError, 'invalid value' if !@separator.nil? && @domainspec.nil?
    end

    def validate_cidrs!
      if @type == :ip6
        raise PermError, 'invalid cidr6' if @cidr6

        if @cidr4
          @cidr6 = "/#{@cidr4}"
          @cidr4 = nil
        end
      end

      raise PermError, "invalid cidr4: #{@cidr4}" if @cidr4 && (@cidr4[1..-1].to_i > 32 || @cidr4.match?(%r{^/0.}))
      raise PermError, "invalid cidr6: #{@cidr6}" if @cidr6 && (@cidr6[2..-1].to_i > 128 || @cidr6.match?(%r{^//0.}))
    end

    def validate_domainspec!
      return if @type == :ptr && @domainspec.nil?

      raise PermError, "invalid domain-spec: #{@domainspec}" if @domainspec.nil?

      # skip dot checks for unknown modifiers
      if (MECHANISMS + MODIFIERS).include?(@type)
        @domainspec = @domainspec[0..-2] if @domainspec[-1] == '.'
        labels = @domainspec.split '.', -1

        valid = labels.all? do |l|
          l.match?(/^%{/) || (
            labels.length > 1 &&
            !l.empty? &&
            !l.include?("\0") &&
            !l.match?(/:/) &&
            l[0] != '-' &&
            l.match?(/[a-z]/)
          )
        end

        raise PermError, "invalid domain-spec: #{@domainspec}" unless valid
      end

      @domain = expand_domain @domainspec
    end

    def validate_ip4!
      raise PermError, 'invalid ip4-network' if @domainspec.nil?
      raise PermError, 'invalid cidr' unless @cidr6.nil?

      begin
        @ip = IPAddr.new "#{@domainspec}#{@cidr4}"
      rescue IPAddr::Error => e
        raise PermError, "invalid ip4-network(#{@domainspec}): #{e}"
      end
    end

    def validate_ip6!
      raise PermError, 'invalid ip6-network' if @domainspec.nil?
      raise PermError, 'invalid cidr' unless @cidr4.nil?

      cidr = @cidr6&.slice(1..-1)

      begin
        @ip = IPAddr.new "#{@domainspec}#{cidr}"
      rescue IPAddr::Error => e
        raise PermError, "invalid ip6-network(#{@domainspec}): #{e}"
      end
    end

    # macro expansion
    REGEXP_TXT = %r{^([slodiphcrtv])([[:digit:]]+)?(r)?([-.+,/_=]+)?$}i.freeze
    REGEXP_DMN = %r{^([slodiphv])([[:digit:]]+)?(r)?([-.+,/_=]+)?$}i.freeze

    def expand_domain(domainspec)
      domain = expand domainspec, REGEXP_DMN
      while domain.length > 253
        parts = domain.split '.'
        domain = parts[1..-1].join '.'
      end
      domain
    end

    def expand(domainspec, regex = REGEXP_TXT)
      raise PermError, 'invalid ascii' unless domainspec.ascii_only?

      return domainspec unless domainspec.include?('%')

      chars = domainspec.chars
      index = 0
      result = +''

      loop do
        break unless chars[index]

        # normal labels
        while chars[index] && chars[index] != '%'
          result << chars[index]
          index += 1
        end
        break unless chars[index]

        # macros
        case chars.slice(index, 2).join
        when '%%'
          result << '%'
          index += 2
        when '%-'
          result << '%20'
          index += 2
        when '%_'
          result << ' '
          index += 2
        when '%{'
          label = +''
          index += 2
          while chars[index] && chars[index] != '}'
            label << chars[index]
            index += 1
          end
          raise PermError, "invalid domain-spec: #{@domainspec}" if chars[index] != '}'

          result << substitute(label.downcase, regex)
          index += 1
        else
          raise PermError, "invalid domain-spec: #{@domainspec}"
        end
      end

      result
    end

    def substitute(label, regex)
      _all, letter, digit, reverse, delimiters = * label.match(regex)
      raise PermError, "invalid macro-letter: #{label}" unless letter

      res = @env.instance_variable_get("@#{letter}") || 'unknown'

      if letter == 'p'
        client_addr = IPAddr.new @env.instance_variable_get '@c'
        domains = @dns.ptr(client_addr.reverse)
        ips = domains.map { |ptr| client_addr.ipv4? ? @dns.a(ptr) : @dns.aaaa(ptr) }
        ips = ips.flatten.map { |ip| IPAddr.new(ip) }
        res = domains.first if ips.include? client_addr.to_s
      end

      res = res.gsub(/[#{delimiters}]/, '.') if delimiters
      res = res.to_s.split('.').reverse.join('.') if reverse
      res = res.to_s.split('.').last(digit.to_i).join('.') if digit
      res = URI.encode_www_form_component(res) if res.match(/=/)
      res
    end
  end
end
