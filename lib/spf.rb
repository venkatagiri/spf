# frozen_string_literal: true

require_relative 'spf/version'

require_relative 'spf/dns'
require_relative 'spf/term'
require_relative 'spf/evaluator'

# Checks the SPF policy
module SPF
  # Helper method to check host
  def self.check_host(*args)
    Evaluator.new.check_host(*args)
  end
end
