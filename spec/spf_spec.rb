# frozen_string_literal: true

require 'resolv'
require 'yaml'
require 'spf'

RSpec.describe 'SPF' do
  before do
    allow_any_instance_of(Resolv::DNS).to receive(:getresources) do |_dns, domain, typeclass|
      zones = zonedata.transform_keys(&:downcase)[domain.downcase] || []
      type = typeclass.is_a?(String) ? typeclass : typeclass.name.sub(/.*::/, '')

      resources = zones.map do |z|
        next unless z.is_a?(Hash) && z[type] && z[type] != 'NONE'

        val = z[type]

        case type
        when 'MX'
          Resolv::DNS::Resource::MX.new(*val)
        when 'A'
          Resolv::DNS::Resource::IN::A.new(val)
        when 'AAAA'
          Resolv::DNS::Resource::IN::AAAA.new(val)
        when 'TXT', 'SPF'
          Resolv::DNS::Resource::TXT.new(*val)
        when 'PTR'
          Resolv::DNS::Resource::PTR.new(*val)
        end
      end.compact

      if !resources.empty?
        resources
      elsif zones.include? 'TIMEOUT'
        raise SPF::TempError, 'dns timeout'
      else
        []
      end
    end
  end

  YAML.load_stream(File.read('spec/fixtures/files/rfc7208-tests-2014-05.yml')).each do |suite|
    context "with suite '#{suite['description']}'" do
      let(:zonedata) { suite['zonedata'] }

      suite['tests'].each do |name, details|
        it "should clear test '#{name}'" do
          skip if details['skip']

          details['host'] = ":#{details['host']}" if details['host'].is_a?(Symbol)
          result, explanation = SPF.check_host(details['host'], details['helo'], details['mailfrom'])

          expect(result).to eq(details['result'])
          expect(explanation).to eq(details['explanation']) if details['explanation']
        end
      end
    end
  end
end
